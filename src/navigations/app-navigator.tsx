import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import PokemonList from '../scenes/pokemon-list';
import PokemonDetail from '../scenes/pokemon-detail';
import capitalize from '../utils/capitalize';

const Stack = createStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="pokemon-list"
          component={PokemonList}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="pokemon-detail"
          component={PokemonDetail}
          options={({route}) => ({
            title: `Pokémon | N° ${route.params.data.id}`,
            headerStyle: {
              backgroundColor: '#616161',
            },
            headerTintColor: '#FFF',
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
