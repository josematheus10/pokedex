import React from 'react';
import AppNavigator from './navigations/app-navigator';

const App = () => <AppNavigator />;

export default App;
