# POKÉDEX
TODOS:
- [x] Listar todos os Pokémons
- [x] Exibir informaçãoes Básicas 
- [ ] Pesquisar Pokémons
- [ ] Salvar Pokémons Favoritos
- [ ] Gráfico de Status (HP, Força, etc..)
- [ ] Filtros de pesquisa
- [ ]  Configurar Splash screen e icones
- [ ]  Otimizar os Assets

# APK DE TESTES 👇
![APK](app-debug.apk)