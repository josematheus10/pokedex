import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import PokemonApi from '../../api/pokeapi.co/v2';
import useDeviceOrientation from '../../utils/useDeviceOrientation';

const PokemonList = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [pageLimit, setpageLimit] = useState(0);
  const maxRecord = 10;
  const api = new PokemonApi(maxRecord);

  const orientation = useDeviceOrientation();

  useEffect(() => {
    //TODO: error tast

    api
      .getPokemonList(0)
      .then(json => {
        setData(json);
        setPage(maxRecord);
        setpageLimit(api.totalRecords);
      })
      .catch(error => console.error(error))
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const loadMore = () => {
    if (page <= pageLimit) {
      api
        .getPokemonList(page)
        .then(json => {
          setData(data.concat(json));
          setPage(page + maxRecord);
        })
        .catch(error => console.error(error));
    }
  };

  const renderItem = ({item}) => {
    const uri = item.details.sprites.other['official-artwork'].front_default;
    const name = item.name;
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={() =>
          navigation.navigate('pokemon-detail', {data: item.details})
        }>
        <Image source={{uri: uri}} style={styles.image} />
        <Text style={styles.text}>{name}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          contentContainerStyle={{padding: 8}}
          style={styles.list}
          data={data}
          keyExtractor={item => item.details.id}
          renderItem={renderItem}
          key={orientation}
          numColumns={orientation === 'portrait' ? 2 : 4}
          onEndReachedThreshold={0.4}
          onEndReached={loadMore}
          removeClippedSubviews={true}
          initialNumToRender={2}
          maxToRenderPerBatch={10}
          updateCellsBatchingPeriod={100}
          windowSize={10}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  list: {
    backgroundColor: '#F2F2F2',
  },
  item: {
    alignItems: 'center',
    backgroundColor: '#fff',
    flexGrow: 1,
    flexBasis: 0,
    margin: 5,
    padding: 20,
    minHeight: 180,
    maxHeight: 180,
    borderRadius: 200,
    borderWidth: 0.5,
    borderColor: '#a4a4a4',
  },
  image: {
    width: 110,
    height: 110,
  },
  text: {
    textAlign: 'center',
    textTransform: 'capitalize',
    fontSize: 18,
  },
});

export default PokemonList;
