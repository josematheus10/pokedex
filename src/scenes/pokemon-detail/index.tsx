import React, {useEffect, useState} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import PokemonTypeList from '../../components/pokemon-types';
import capitalize from '../../utils/capitalize';

const PokemonDetail = ({route}) => {
  const data = route.params.data;

  const Section: React.FC<{
    title: string;
  }> = ({children, title}) => {
    return (
      <View style={styles.sectionContainer}>
        <Text style={styles.sectionDescription}>{title}</Text>
        <Text style={styles.sectionTitle}>{children}</Text>
      </View>
    );
  };

  const getTypeList = (): string[] => {
    const typeList: string[] = [];
    data.types.forEach(element => {
      typeList.push(element.type.name);
    });

    return typeList;
  };

  const image = data.sprites.other['official-artwork'].front_default;
  const typeList = getTypeList();
  // TODO: refactor here
  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <ImageBackground
          source={require('../../assets/images/background.png')}
          style={styles.background}
          imageStyle={styles.bg}>
          <View style={styles.imageContainer}>
            <Image source={{uri: image}} style={styles.image} />
          </View>
          <Text style={styles.text}>{capitalize(data.name)}</Text>
        </ImageBackground>
        <PokemonTypeList types={typeList}></PokemonTypeList>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <Section title="Peso">{data.weight / 10} kg</Section>
          <Section title="Altura">{data.height / 10} m</Section>
        </View>
        <View style={[styles.sectionContainer, {marginTop: 12}]}>
          <Text style={styles.sectionDescription}>Habilidades</Text>
          {data.abilities.map(r => {
            console.log(r);
            if (!r.is_hidden) {
              return (
                <Text key={r.ability.name} style={styles.sectionTitle}>
                  {r.ability.name}
                </Text>
              );
            }
          })}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  background: {
    paddingBottom: 18,
    paddingTop: 30,
    minHeight: 200,
    backgroundColor: '#FFFF',
    alignItems: 'center',
  },
  bg: {
    overflow: 'visible',
    resizeMode: 'cover',
  },
  text: {
    fontSize: 40,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#FFF',
  },
  imageContainer: {
    alignItems: 'center',
    backgroundColor: '#616161',
    minWidth: 250,
    minHeight: 250,
    maxWidth: 250,
    maxHeight: 250,
    borderRadius: 200,
    borderWidth: 4,
    borderColor: '#FFF',
    justifyContent: 'center',
  },
  image: {
    width: 250,
    height: 250,
  },
  sectionContainer: {
    marginTop: 5,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
});

export default PokemonDetail;
