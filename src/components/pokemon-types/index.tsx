import React from 'react';
import {StyleSheet, Text, StatusBar, FlatList, Image, View} from 'react-native';
import pokemonTypesImages from './pokemonTypesImages';
import pokemonTypesColor from './pokemonTypesStyle';

const renderItem = ({item}) => {
  return (
    <View style={[styles.item, {backgroundColor: pokemonTypesColor[item]}]}>
      <Image source={pokemonTypesImages[item]} style={styles.image} />
    </View>
  );
};

const PokemonTypeList: React.FC<{types: string[]}> = ({types}) => {
  return (
    <FlatList
      horizontal
      contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}
      showsHorizontalScrollIndicator={false}
      renderItem={renderItem}
      data={types}
      keyExtractor={item => item}
    />
  );
};

const styles = StyleSheet.create({
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
    borderRadius: 200,
    borderWidth: 4,
    borderColor: '#FFF',
    minWidth: 80,
    minHeight: 80,
    maxWidth: 80,
    maxHeight: 80,
    margin: 10,
    marginVertical: 15,
  },
  image: {
    width: 50,
    height: 50,
  },
});

export default PokemonTypeList;
