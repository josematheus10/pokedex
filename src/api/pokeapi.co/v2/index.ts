export default class PokemonApi {
  private _limit: number;
  private _totalRecords = 0;

  constructor(limit = 10) {
    this._limit = limit;
  }

  get totalRecords() {
    return this._totalRecords;
  }

  //Todo: Type here
  private getPokemon(offset = 0): Promise<any> {
    return this.requestJson(
      `https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=${this._limit}`,
    );
  }

  async getPokemonList(offset: number) {
    const {results, count} = await this.getPokemon(offset);

    const promises = results.map(async (element: any) => {
      await this.requestJson(element.url).then(details => {
        element['details'] = details;
      });
    });

    if (this._totalRecords === 0) {
      this._totalRecords = count - 1;
    }

    await Promise.all(promises);
    return results;
  }

  private requestJson(url: string): Promise<any> {
    return fetch(url).then(response => response.json());
  }
}
