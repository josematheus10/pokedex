const pokemonTypesImages = {
  normal: require('../../assets/images/pokemon-types-icons/normal.png'),
  fighting: require('../../assets/images/pokemon-types-icons/fighting.png'),
  flying: require('../../assets/images/pokemon-types-icons/flying.png'),
  poison: require('../../assets/images/pokemon-types-icons/poison.png'),
  ground: require('../../assets/images/pokemon-types-icons/ground.png'),
  rock: require('../../assets/images/pokemon-types-icons/rock.png'),
  bug: require('../../assets/images/pokemon-types-icons/bug.png'),
  ghost: require('../../assets/images/pokemon-types-icons/ghost.png'),
  steel: require('../../assets/images/pokemon-types-icons/steel.png'),
  fire: require('../../assets/images/pokemon-types-icons/fire.png'),
  water: require('../../assets/images/pokemon-types-icons/water.png'),
  grass: require('../../assets/images/pokemon-types-icons/grass.png'),
  electric: require('../../assets/images/pokemon-types-icons/electric.png'),
  psychic: require('../../assets/images/pokemon-types-icons/psychic.png'),
  ice: require('../../assets/images/pokemon-types-icons/ice.png'),
  dragon: require('../../assets/images/pokemon-types-icons/dragon.png'),
  fairy: require('../../assets/images/pokemon-types-icons/fairy.png'),
  dark: require('../../assets/images/pokemon-types-icons/dark.png'),
};

export default pokemonTypesImages;
